
FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin

# Change `app` to whatever your binary is called
Add app .
CMD ["./app"]
